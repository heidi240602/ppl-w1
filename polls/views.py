from django.shortcuts import render
from .models import Visitor
from .forms import VisitorForm
from django.http import HttpResponse, HttpResponseRedirect

def home(request):
    visitors = Visitor.objects.all() # TODO Implement this
    response = {'visitors': visitors}
    return render(request, 'home.html', response)

def create_visitor(request):
    if request.method == 'POST':
        form = VisitorForm(request.POST)
        if form.is_valid():
            form.save()  # Save data to DB
            return HttpResponseRedirect('/polls/home')  # Redirect on finish

    # if a GET (or any other method) we'll create a blank form
    else:
        form = VisitorForm()

    return render(request, 'form.html', {'form': form})