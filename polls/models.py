from django.db import models

class Visitor(models.Model):
    def __str__(self):
        return self.name
    name = models.CharField(max_length=255)
    age=models.IntegerField()
